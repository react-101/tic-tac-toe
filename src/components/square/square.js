import React from 'react';
export default function Square(props) {
  console.log(props);
  return (
    <button className={!props.className ? 'square' : `square ${props.className}`} onClick={props.onClick}>
      {props.value}
    </button>
  );
}
