import React from 'react';
import Board from '../board/board';

export default class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null),
        },
      ],
      xIsNext: true,
      stepNumber: 0,
      toggleSort: false,
      winnerSquares: [],
    };
    // this.handleToggleSort = this.handleToggleSort.bind(this);
    // this.handleClick = this.handleClick.bind(this);
  }

  handleClick(i) {
    console.log(`handleClick fire`);
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const newSquares = current.squares.slice();

    if (calculateWinner(newSquares)[0] || newSquares[i]) {
      // this.forceUpdate();
      return;
    }

    newSquares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{ squares: newSquares }]),
      xIsNext: !this.state.xIsNext,
      stepNumber: history.length,
    });
  }

  jumpTo(step, history) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0,
    });
  }

  handleToggleSort() {
    this.setState({ toggleSort: !this.state.toggleSort });
  }

  componentDidUpdate() {}
  render() {
    const history = this.state.history;

    const current = history[this.state.stepNumber];
    const [winner, winnerSquares] = calculateWinner(current.squares);
    // console.log(winner);
    // console.log(winnerSquares);

    let moves = history.map((move, step, history) => {
      const desc = step ? `Перейти к ходу ${step}` : `К началу игры`;

      const coordinates = step ? getCoordinates(step, history) : [0, 0];
      return (
        <li key={step}>
          <button onClick={() => this.jumpTo(step, history)}>
            {step === this.state.stepNumber ? <b>{desc}</b> : desc}{' '}
          </button>
          <span> - {coordinates}</span>
        </li>
      );
    });
    let status = winner
      ? `Выиграл игрок: ${this.state.xIsNext ? 'O' : 'X'}`
      : current.squares.includes(null)
      ? `Next player: ${this.state.xIsNext ? 'X' : 'O'}`
      : `Ничья`;

    return (
      <div className="game">
        <div className="game-board">
          <Board squares={current.squares} onClick={i => this.handleClick(i)} winnerSquares={winnerSquares} />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <button onClick={() => this.handleToggleSort()}>sort</button>
          <ol reversed={this.state.toggleSort}>{this.state.toggleSort ? moves.reverse() : moves}</ol>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares, xIsNext) {
  const lines = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
  let winnerSquares = [];
  const winner = lines.some(e => {
    const [a, b, c] = e;
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      winnerSquares = [].concat([a, b, c]);
      return true;
    }
    return false;
  });
  console.log(winnerSquares);
  return [winner, winnerSquares];
}

function getCoordinates(step, history) {
  const locations = [[1, 1], [2, 1], [3, 1], [1, 2], [2, 2], [3, 2], [1, 3], [2, 3], [3, 3]];
  const current = history[step].squares;
  const prev = history[step - 1].squares;

  for (let i = 0; i <= current.length; i++) {
    if (current[i] !== prev[i]) {
      return locations[i];
    }
  }
}
