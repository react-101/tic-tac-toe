import React from 'react';
import Square from '../square/square';

export default class Board extends React.Component {
  renderSquare(i) {
    const isWinnerSquare = this.props.winnerSquares.includes(i) ? 'winnerSquare' : null;
    console.log(isWinnerSquare);
    return (
      <Square
        key={i}
        className={isWinnerSquare}
        value={this.props.squares[i]}
        onClick={() => {
          return this.props.onClick(i);
        }}
      />
    );
  }

  renderBoardRow(props) {
    return [[0, 1, 2], [3, 4, 5], [6, 7, 8]].map((e, i) => {
      return (
        <div key={i} className="board-row">
          {e.map(i => this.renderSquare(i))}
        </div>
      );
    });
  }

  render() {
    console.log(this.props.winner);
    let field = [];

    return (
      <div>
        <div className="status">{/*REVIEW здесь был status до поднятия состояния в Game*/}</div>
        {field}
        {this.renderBoardRow()}
      </div>
    );
  }
}
